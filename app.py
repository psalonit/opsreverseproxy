import itertools
import os

from flask import Flask, Response, request
import requests

import settings

app = Flask(__name__)

sample_responses_path = os.path.join(os.getcwd(), 'sample_responses')
filenames = next(os.walk(sample_responses_path), (None, None, []))[2]


def get_next_response_file():
    for f in itertools.cycle(filenames):
        yield f


file_generator = get_next_response_file()


@app.route('/nyx/account/accountweb.dll', methods=['GET', 'POST'])
def nyx_accountweb():
    response = {}
    request_to_mock = settings.REQUEST_TO_MOCK
    response_file = settings.RESPONSE_FILE
    ops_endpoint = settings.OPS_ENDPOINT

    if request.values.get('request') == request_to_mock or request_to_mock in str(request.data):
        with open(os.path.join(sample_responses_path, response_file)) as f:
            response = Response(
                response=f.read(),
                status=200,
                mimetype="application/xml",
                # headers=ops_response.headers
            )
    else:
        ops_response = requests.post(
            ops_endpoint,
            params=request.query_string,
            headers=request.headers,
            data=request.data
        )
        response = Response(
            response=ops_response.text,
            status=200,
            mimetype="application/xml",
            # headers=ops_response.headers
        )
    print(request.values.get('request'), request.query_string, request.data)
    print('Response:', getattr(response, 'response', ''))
    return response


if __name__ == '__main__':
    app.run()
