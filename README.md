# README #

### What is this repository for? ###

A dirty simple flask project that is used to mock OPS responses.


### How do I get set up? ###

* Change the following settings in the settings file
~~~~
REQUEST_TO_MOCK = 'Login'
RESPONSE_FILE = 'Error1.xml'
OPS_ENDPOINT = 'https://spapi.goldenmi-test.ops.sgd-cloud.com/nyx/account/accountweb.dll'
~~~~
* Write the response you expect on the response file
* Set the OPS endpoint for the module you are testing 
(Portal, Thor or Baldur) to the flask service url (e.g., http://127.0.0.1:5000/ )
* Now the flask service runs on your host on http://127.0.0.1:5000/ and we want to access it from our VM where our app
(e.g., Portal) runs. In order to achieve that we need to create an ssh reverse tunnel from the VM to our host
````
ssh -R 5555:127.0.0.1:5000 gpapp@vagrant
````
This will make the flask service available from VM at http://127.0.0.1:5555
* All requests will go on normally expect from the one you have set as a _REQUEST_TO_MOCK_
* Extend the _nyx_accountweb_ function to add any needed logic

### Who do I talk to? ###
panagiotis.salonitis@sgdigital.com